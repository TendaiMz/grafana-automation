 variable "GRAFANA_URL" {
  description = "Grafana endpoint"
  type        = string  
}

variable "GRAFANA_AUTH_TOKEN" {
  description = "Grafana auth token"
  type        = string  
}



